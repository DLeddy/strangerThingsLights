#!/usr/bin/python

# Author: Damien Leddy(dam.leddy@gmail.com)
import time
import random
from neopixel import *

from ST.TwitterReader import TwitterReader
from ST.StdOutListener import StdOutListener
from ST.StrangerLights import StrangerLights

tag = "#gogostrangerlights"



strangerLights = StrangerLights()
print ('Press Ctrl-C to quit.')
listener = StdOutListener(strangerLights)
twitterReader = TwitterReader()
print ('stream start')
twitterReader.stream(listener,tag)
print ('stream running')
#initialize all the lights
#strangerLights.initLights()    time.sleep(random.randint(5,15))
