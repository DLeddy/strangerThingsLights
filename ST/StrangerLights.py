#!/usr/bin/python

# Stranger Things Christmas Lights
# Originial Author: Paul Larson (djhazee@gmail.com)
# (https://github.com/djhazee/strangerlights)
#
# Author of this Modification: Damien Leddy(dam.leddy@gmail.com)
# -Port of the Arduino NeoPixel library strandtest example (Adafruit).
# -Uses the WS2811 to animate RGB light strings (I am using a 5V, 50x RGB LED strand)
# -This will blink a designated light for each letter of the alphabet

# Import libs used
import time
import random
from neopixel import *

class StrangerLights(object):

  def __init__(self):
    #Start up random seed
    random.seed()

    # LED strip configuration:
    self.LED_COUNT      = 50      # Number of LED pixels.
    self.LED_PIN        = 18      # GPIO pin connected to the pixels (must support PWM!).
    self.LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
    self.LED_DMA        = 5       # DMA channel to use for generating signal (try 5)
    self.LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
    self.LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)



    #Predefined Colors and Masks
    self.OFF = Color(0,0,0)
    self.WHITE = Color(255,255,255)
    self.RED = Color(255,0,0)
    self.GREEN = Color(0,255,0)
    self.BLUE = Color(0,0,255)
    self.PURPLE = Color(128,0,128)
    self.YELLOW = Color(255,255,0)
    self.ORANGE = Color(255,50,0)
    self.TURQUOISE = Color(64,224,208)
    self.RANDOM = Color(random.randint(0,255),random.randint(0,255),random.randint(0,255))

    #list of colors, tried to match the show as close as possible
    self.COLORS = [self.YELLOW,self.GREEN,self.RED,self.BLUE,self.ORANGE,self.TURQUOISE,self.GREEN,
          self.YELLOW,self.PURPLE,self.RED,self.GREEN,self.BLUE,self.YELLOW,self.RED,self.TURQUOISE,self.GREEN,self.RED,self.BLUE,self.GREEN,self.ORANGE,
          self.YELLOW,self.GREEN,self.RED,self.BLUE,self.ORANGE,self.TURQUOISE,self.RED,self.BLUE, 
          self.ORANGE,self.RED,self.YELLOW,self.GREEN,self.PURPLE,self.BLUE,self.YELLOW,self.ORANGE,self.TURQUOISE,self.RED,self.GREEN,self.YELLOW,self.PURPLE,
          self.YELLOW,self.GREEN,self.RED,self.BLUE,self.ORANGE,self.TURQUOISE,self.GREEN,self.BLUE,self.ORANGE] 


    #bitmasks used in scaling RGB values
    self.REDMASK = 0b111111110000000000000000
    self.GREENMASK = 0b000000001111111100000000
    self.BLUEMASK = 0b000000000000000011111111

    # Other vars
    self.ALPHABET = '*******abcdefghijklm********zyxwvutsrqpon*********'  #alphabet that will be used
    self.LIGHTSHIFT = 0  #shift the lights down the strand to the other end 
    self.FLICKERLOOP = 3  #number of loops to flicker

    # Create NeoPixel object with appropriate configuration.
    self.strip = Adafruit_NeoPixel(self.LED_COUNT, self.LED_PIN, self.LED_FREQ_HZ, self.LED_DMA, self.LED_INVERT, self.LED_BRIGHTNESS)
    self.strip.begin()


  def initLights(self):
    """
    initializes the light strand colors 

    inputs: 
      strip = color strip instance to action against

    outputs:
      <none>
    """
    colorLen = len(self.COLORS)
    #Initialize all LEDs
    for i in range(len(self.ALPHABET)):
      self.strip.setPixelColor(i+self.LIGHTSHIFT, self.COLORS[i%colorLen])
    self.strip.show()

  def blinkWords(self,word):
    """
    blinks a string of letters

    inputs: 
      strip = color strip instance to action against
      word = word to blink

    outputs:
      <none>
    """
    #create a list of jumbled ints
    s = list(range(len(self.ALPHABET)))
    random.shuffle(s)

    #first, kill all lights in a semi-random fashion
    for led in range(len(self.ALPHABET)):
      self.strip.setPixelColor(s[led]+self.LIGHTSHIFT, self.OFF)
      self.strip.show()
      time.sleep(random.randint(10,80)/1000.0)

    #quick delay
    time.sleep(1.75)

    #if letter in alphabet, turn on 
    #otherwise, stall
    for character in word:
      if character in self.ALPHABET:
        self.strip.setPixelColor(self.ALPHABET.index(character)+self.LIGHTSHIFT, self.RED)
        self.strip.show()
        time.sleep(1)
        self.strip.setPixelColor(self.ALPHABET.index(character)+self.LIGHTSHIFT, self.OFF)
        self.strip.show()
        time.sleep(.5)
      else:
        time.sleep(.75)

  def flicker(self):
    """
    creates a flickering effect on a bulb

    inputs: 
      strip = color strip instance to action against
      ledNo = LED position on strand, as integer.

    outputs:
      <none>
    """
    #pick LED
    ledNo = random.randint(self.LIGHTSHIFT,len(self.ALPHABET)+self.LIGHTSHIFT)
    #get origin LED color
    origColor = self.strip.getPixelColor(ledNo)

    #do FLICKERLOOP-1 loops of flickering  
    for i in range(0,self.FLICKERLOOP-1):

      #get current LED color, break out to individuals
      currColor = self.strip.getPixelColor(ledNo)
      currRed = (currColor & self.REDMASK) >> 16
      currGreen = (currColor & self.GREENMASK) >> 8
      currBlue = (currColor & self.BLUEMASK)

      #turn off for a random short period of time
      self.strip.setPixelColor(ledNo, self.OFF)
      self.strip.show()
      time.sleep(random.randint(10,50)/1000.0)

      #turn back on at random scaled color brightness
      #modifier = random.randint(30,120)/100
      modifier = 1
      #TODO: fix modifier so each RGB value is scaled. 
      #      Doesn't work that well so modifier is set to 1. 
      newBlue = int(currBlue * modifier)
      if newBlue > 255:
        newBlue = 255
      newRed = int(currRed * modifier)
      if newRed > 255:
        newRed = 255
      newGreen = int(currGreen * modifier) 
      if newGreen > 255:
        newGreen = 255
      self.strip.setPixelColor(ledNo, Color(newRed,newGreen,newBlue))
      self.strip.show()
      #leave on for random short period of time
      time.sleep(random.randint(10,80)/1000.0)

    #restore original LED color
    self.strip.setPixelColor(ledNo, origColor)

  def runBlink(self):
    """
    blinks the RUN letters

    inputs: 
      strip = color strip instance to action against

    outputs:
      <none>
    """
    word = "run"
    #first blink the word "run", one letter at a time
    self.blinkWords(word)

    #now frantically blink all 3 letters
    for loop in range(20):
      #turn on all three letters at the same time
      for character in word:
        if character in self.ALPHABET:
          self.strip.setPixelColor(self.ALPHABET.index(character)+self.LIGHTSHIFT, self.RED)
      self.strip.show()

      time.sleep(random.randint(15,100)/1000.0)

      #turn off all three letters at the same time
      for character in word:
        if character in self.ALPHABET:
          self.strip.setPixelColor(self.ALPHABET.index(character)+self.LIGHTSHIFT, self.OFF)
      self.strip.show()

      time.sleep(random.randint(50,150)/1000.0)

    #now frantically blink all lights 
    for loop in range(15):
      #initialize all the lights
      self.initLights()

      time.sleep(random.randint(50,150)/1000.0)

      #kill all lights
      self.killLights()

      time.sleep(random.randint(50,150)/1000.0)


  def killLights(self):
    #kill all lights
    for led in range(len(self.ALPHABET)):
      self.strip.setPixelColor(led+self.LIGHTSHIFT, self.OFF)
    self.strip.show()


  def initialiseAllLights(self):
    #Initialize all LEDs
    #turn on each light in a semi-random fasion
    colorLen = len(self.COLORS)
    for i in range(len(self.ALPHABET)):
      self.strip.setPixelColor(s[i]+self.LIGHTSHIFT, self.COLORS[s[i]%colorLen])
      self.strip.show()
      time.sleep(random.randint(10,80)/1000.0)
