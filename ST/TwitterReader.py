#!/usr/bin/python

# Stranger Things Christmas Lights
# Originial Author: Paul Larson (djhazee@gmail.com)
# (https://github.com/djhazee/strangerlights)
#

conf = "config.conf"

from .StdOutListener import StdOutListener
from tweepy import OAuthHandler
from tweepy import Stream
import os
import json

class TwitterReader(object):

	def __init__(self):
		self.consumer_key = self.readFile(conf,'consumer_key')
		print(self.consumer_key)
		self.consumer_secret = self.readFile(conf,'consumer_secret')
		print(self.consumer_secret)
		self.access_token = self.readFile(conf,'access_token')
		print(self.access_token)
		self.access_token_secret = self.readFile(conf,'access_token_secret')
		print(self.access_token_secret)
		self.text = "stranger Things:"
		self.auth = ""

	def lights(self,strng):
		print(self.text, " ", strng)

	def readFile(self,filename,val):
		with open(os.path.join(os.path.dirname(__file__),conf), 'r') as f:
			config = json.load(f)
			if val in config:
				return config[val] 
			else:
				return "NOT_FOUND"

	def stream(self,listener,tag):
		self.auth = OAuthHandler(self.consumer_key, self.consumer_secret)
		self.auth.set_access_token(self.access_token, self.access_token_secret)
		stream = Stream(self.auth, listener)
		stream.filter(track=[tag])


    