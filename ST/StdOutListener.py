from tweepy.streaming import StreamListener
import json

class StdOutListener(StreamListener):
	def __init__(self,arg):
		self.strangerLights = arg

	def on_data(self, data):
		print('a')
		tag = "#gogostrangerlights"
		word = json.loads(data)['text']
		print(word)
		word = word[len(tag):]

		self.strangerLights.killLights()
		self.strangerLights.blinkWords(word)
		self.strangerLights.runBlink()
		self.strangerLights.killLights()
		return True

	def on_status(self, status):
		print('b')
		print(status.text)

	def on_error(self, status):
		print('c')
		print(status)
		return False

